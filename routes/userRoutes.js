const express = require('express');
const userController = require('../controllers/userController.js');
const auth = require('../auth.js')
const router = express.Router();

// Routes:
//checkEmail Route
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

//Register Route - Create User in Db Collection
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/details", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

// Route for enrolling a user
router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		res.send("Your are an Admin you cannot enroll.")
	} else {
		let data = {
			userId: userData.id,
			// isAdmin: userData.isAdmin,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	}
})




module.exports = router;