const Course = require('../models/Course');
const auth = require('../auth');
const User = require('../models/User');

// Create a new course
module.exports.addCourse = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if(userData.isAdmin == false){
            return "You are not an Admin"
        }else{

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if(error) {
			return false
		} else {
			return "Course Creation Successful"
		}
	})
}
})
}

// Controller function for retreiving all courses
module.exports.getAllCourses = (data) => {
	if(data.isAdmin){
		return Course.find({}).then(result => {
			return result
		})
	}else{
		return false
	}
};

// Retrieves All Active Courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
};

// Retrieve a specific Course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
};

// Update a Course
module.exports.updateCourse = (reqParams, reqBody, data) => {
	if(data) {
		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
		
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error)=> {
			if(error){
				return false
			}else{
				return true
			}
		})
		
	}else{
		return " You are not an Admin"
	}
}

//Update isActive
module.exports.updateIsActive = (reqParams, reqBody, data) => {
	if (data){
		let updateIsActive = {
			isActive: reqBody.isActive
		}
		return Course.findByIdAndUpdate(reqParams.courseId, updateIsActive).then((updateIsActive, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	}else{
		return "Your are not an Admin"
	}
};