
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors'); 
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

const app = express();

const port = process.env.PORT || 4000;

// Middlewares:
app.use(cors()); // allows all resources to access our backend application
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Main URI:
app.use("/users", userRoutes)
app.use("/courses", courseRoutes)


// Mongoose Connection:
mongoose.connect(`mongodb+srv://nel03:jonsera03@zuitt-batch197.r9sr8xv.mongodb.net/s37-s41?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));


app.listen(port, () => console.log(`API is now online at port: ${port}`));